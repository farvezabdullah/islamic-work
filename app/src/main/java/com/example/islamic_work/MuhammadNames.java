package com.example.islamic_work;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MuhammadNames extends AppCompatActivity {
    ListView listView;
    String [] rowBanglaTitel;
    String [] rowArabicTitell;
    String [] rowBanglaDiscriptionl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("মুহাম্মদ  ৯৯ নাম");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_allah_name);

        listView=findViewById(R.id.allahlistId);
        rowBanglaTitel=getResources().getStringArray(R.array.MnameBangla);
        rowArabicTitell=getResources().getStringArray(R.array.MnameArabic);
        rowBanglaDiscriptionl=getResources().getStringArray(R.array.MnameMening);

        MyAdapter myAdapter=new MyAdapter(this,rowBanglaTitel,rowArabicTitell,rowBanglaDiscriptionl);

        listView.setAdapter(myAdapter);
    }
    class MyAdapter extends ArrayAdapter<String> {
        Context context;
        String [] bangla;
        String[] english;
        String[] mening;


        MyAdapter(Context c,String [] rowBanglaTitel, String[] rowArabicTitell,String[] rowBanglaDiscriptionl) {
            super(c, R.layout.row_muhammad,R.id.rowMbanblalId,rowBanglaTitel);
            this.context=c;
            this.bangla=rowBanglaTitel;
            this.english=rowArabicTitell;
            this.mening=rowBanglaDiscriptionl;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater=(LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View muhammadrow=layoutInflater.inflate(R.layout.row_muhammad,parent,false);
            TextView textView=muhammadrow.findViewById(R.id.rowMbanblalId);
            TextView textView1=muhammadrow.findViewById(R.id.rowMarabicId);
            TextView textView2=muhammadrow.findViewById(R.id.rowMmeningId);

            textView.setText(rowBanglaTitel[position]);
            textView1.setText(rowArabicTitell[position]);
            textView2.setText(rowBanglaDiscriptionl[position]);


            return muhammadrow;
        }
    }
}
