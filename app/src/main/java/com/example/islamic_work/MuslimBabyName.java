package com.example.islamic_work;

import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MuslimBabyName extends AppCompatActivity {

    private TabLayout tabLayout1;
    private ViewPager viewPager1;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_muslim_baby_name);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle("মুসলিম শিশুদের নাম");
        
        tabLayout1=findViewById(R.id.tablayoutId1);
        viewPager1=findViewById(R.id.viewPagerId1);

        viewPager1.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
        //viewPager1.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
        tabLayout1.setupWithViewPager(viewPager1);

        tabLayout1.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager1.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
    class MyPagerAdapter extends FragmentPagerAdapter{

        String[] text={"ছেলে শিশুদের নাম"," মেয়ে শিশুদের নাম"};

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {

            if (i==0){
                return new BoyFragment();
            }
            if (i==1) {
                return new GirlsFragment();

            }


            return null;
        }

        @Override
        public int getCount() {
            return text.length;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return text[position];
        }
    }

}
