package com.example.islamic_work;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class QuranPrays extends AppCompatActivity {
    ListView listView;
    String [] count;
    String [] arabic;
    String [] mening;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("কোরআনের দোয়া সমূহ");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_quran_prays);

        listView=findViewById(R.id.QPlistId);
        count=getResources().getStringArray(R.array.QPcount);
        arabic=getResources().getStringArray(R.array.QParabic);
        mening=getResources().getStringArray(R.array.QPmening);

        MyAdapter myAdapter=new MyAdapter(this,count,arabic,mening);



        listView.setAdapter(myAdapter);
    }
    class MyAdapter extends ArrayAdapter<String> {
        Context context;
        String [] counts;
        String[] arabics;
        String[] menings;

        MyAdapter(Context c,String [] rowCount, String[] rowArabic,String[] rowMening) {
            super(c, R.layout.row_quran_prays,R.id.rowQPcountId,rowCount);
            this.context=c;
            this.counts=rowCount;
            this.arabics=rowArabic;
            this.menings=rowMening;


        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater=(LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View QProw=layoutInflater.inflate(R.layout.row_quran_prays,parent,false);
            TextView textView=QProw.findViewById(R.id.rowQPcountId);
            TextView textView1=QProw.findViewById(R.id.rowQParabicId);
            TextView textView2=QProw.findViewById(R.id.rowQPmeningId);

            textView.setText(count[position]);
            textView1.setText(arabic[position]);
            textView2.setText(mening[position]);

            return QProw;
        }
    }
}
