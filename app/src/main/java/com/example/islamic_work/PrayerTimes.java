package com.example.islamic_work;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class PrayerTimes extends AppCompatActivity {

    private static final String TAG = "tag";
    String url;

    // Tag used to cancel the request
    String tag_json_obj = "json_obj_req";

    ProgressDialog pDialog;

    TextView mFajrTv,mDuhurTv,mAsrTv,mMagribTv,mIsaTv,mLocationTv,mDateTv,mSunriseTv;
    EditText mSearchET;
    Button mSearchBT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prayer_times);


        mFajrTv   =findViewById(R.id.fajrTv);
        mDuhurTv  =findViewById(R.id.duhurTv);
        mAsrTv    =findViewById(R.id.asrTv);
        mMagribTv =findViewById(R.id.magribTv);
        mIsaTv    =findViewById(R.id.isaTv);
        mLocationTv=findViewById(R.id.locationTv);
        mDateTv=findViewById(R.id.dateTv);
        mSunriseTv=findViewById(R.id.sunriseTv);
        mSearchET=findViewById(R.id.searchET);
        mSearchBT=findViewById(R.id.searchBT);

        mSearchBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mLocation=mSearchET.getText().toString().trim();

                if (mLocation.isEmpty()){
                    Toast.makeText(PrayerTimes.this,"Please enter location",Toast.LENGTH_LONG).show();
                }else {
                    url="http://muslimsalat.com/"+mLocation+".json?key=1d8680c67fe8fa48f235058a40f";
                    searchLocation();
                }
            }
        });

    }

    private void searchLocation() {

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String country=response.get("country").toString();
                            String state=response.get("state").toString();
                            String city=response.get("city").toString();
                            String location=country+" "+state+" "+city;

                            String date=response.getJSONArray("items").getJSONObject(0).get("date_for").toString();


                            String mFajr=response.getJSONArray("items").getJSONObject(0).get("fajr").toString();
                            String mDuhur=response.getJSONArray("items").getJSONObject(0).get("dhuhr").toString();
                            String mAsr=response.getJSONArray("items").getJSONObject(0).get("asr").toString();
                            String mMagrib=response.getJSONArray("items").getJSONObject(0).get("maghrib").toString();
                            String mIsa=response.getJSONArray("items").getJSONObject(0).get("isha").toString();
                            String mSunrise=response.getJSONArray("items").getJSONObject(0).get("shurooq").toString();

                            mFajrTv.setText(mFajr);
                            mDuhurTv.setText(mDuhur);
                            mAsrTv.setText(mAsr);
                            mMagribTv.setText(mMagrib);
                            mIsaTv.setText(mIsa);
                            mSunriseTv.setText(mSunrise);

                            mLocationTv.setText(location);
                            mDateTv.setText(date);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        pDialog.hide();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(PrayerTimes.this,"Error",Toast.LENGTH_LONG).show();
                pDialog.hide();
            }
        });

// Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }
}
