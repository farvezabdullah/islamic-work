package com.example.islamic_work;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Tasbeeh_count extends AppCompatActivity {
TextView textView,textView1,textView2;
Button b,b2;
   static int i=0,j=0,p=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("তাসবীহ গণনা");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_tasbeeh_count);
        textView=findViewById(R.id.textShow1);
        textView1=findViewById(R.id.textShow2);
        textView2=findViewById(R.id.textShow3);
   b=(Button)findViewById(R.id.bid1);
        //b2=(Button)findViewById(R.id.bid2);


        textView.setText(Integer.toString(i)+"/33");
        textView1.setText("Total:"+Integer.toString(i));
        textView2.setText("Recited: "+Integer.toString(i)+" times");

        b.setOnClickListener(new View.OnClickListener() {
     @Override
     public void onClick(View v) {

         i++;
             textView.setText(Integer.toString(i)+"/33");
             if(i==33){i=0;
             p++;
                 textView2.setText("Recited:"+Integer.toString(p)+" times");
                 Toast.makeText(getApplication(),"Recited:"+Integer.toString(p)+" times",Toast.LENGTH_LONG).show();
              }
             j++;
             textView1.setText("Total:"+Integer.toString(j));

     }
 });
       /* b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(i>0){
                    i=0;
                    textView.setText(Integer.toString(i)+"/33");

                }if(j>0)
                {
                    j=0;
                    textView1.setText("Total:"+Integer.toString(j));
                }if (p>0)
                {
                    p=0;
                    textView2.setText("Recited: "+Integer.toString(p)+" times");
                }
            }
        });*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tasbeeh,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if (id==R.id.clearId)
        {
            if(i>0){
                i=0;
                textView.setText(Integer.toString(i)+"/33");

            }if(j>0)
        {
            j=0;
            textView1.setText("Total:"+Integer.toString(j));
        }if (p>0)
        {
            p=0;
            textView2.setText("Recited: "+Integer.toString(p)+" times");
        }
        return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
