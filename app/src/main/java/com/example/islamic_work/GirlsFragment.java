package com.example.islamic_work;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;


/**
 * A simple {@link Fragment} subclass.
 */
public class GirlsFragment extends Fragment {


    public GirlsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_girls, container, false);
        WebView webView1=(WebView) view.findViewById(R.id.webGirl);
        webView1.setWebViewClient(new WebViewClient());
        WebSettings webSettings=webView1.getSettings();
        webSettings.setJavaScriptEnabled(true);

        webView1.loadUrl("https://www.fancim.com/bn/%E0%A6%AE%E0%A7%87%E0%A6%AF%E0%A6%BC%E0%A7%87%E0%A6%A6%E0%A7%87%E0%A6%B0-%E0%A6%87%E0%A6%B8%E0%A6%B2%E0%A6%BE%E0%A6%AE%E0%A6%BF%E0%A6%95-%E0%A6%A8%E0%A6%BE%E0%A6%AE-%E0%A6%85%E0%A6%95%E0%A7%8D/");

        return view;
    }

}
