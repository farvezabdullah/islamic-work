package com.example.islamic_work;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class AllahName extends AppCompatActivity {
    ListView listView;
    String [] rowBanglaTitel;
    String [] rowArabicTitell;
    String [] rowBanglaDiscriptionl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("আল্লাহ ৯৯ নাম");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_allah_name);

        listView=findViewById(R.id.allahlistId);
        rowBanglaTitel=getResources().getStringArray(R.array.rowBanglaTitel);
        rowArabicTitell=getResources().getStringArray(R.array.rowArabicTitell);
        rowBanglaDiscriptionl=getResources().getStringArray(R.array.rowBanglaDiscriptionl);

        MyAdapter myAdapter=new MyAdapter(this,rowBanglaTitel,rowArabicTitell,rowBanglaDiscriptionl);

        listView.setAdapter(myAdapter);
    }
    class MyAdapter extends ArrayAdapter<String> {
        Context context;
        String [] bangla;
        String[] english;
        String[] mening;


        MyAdapter(Context c,String [] rowBanglaTitel, String[] rowArabicTitell,String[] rowBanglaDiscriptionl) {
            super(c, R.layout.row_allah,R.id.rowBanglaTitelId,rowBanglaTitel);
            this.context=c;
            this.bangla=rowBanglaTitel;
            this.english=rowArabicTitell;
            this.mening=rowBanglaDiscriptionl;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater=(LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View allahrow=layoutInflater.inflate(R.layout.row_allah,parent,false);
            TextView textView=allahrow.findViewById(R.id.rowBanglaTitelId);
            TextView textView1=allahrow.findViewById(R.id.rowArabicTitellId);
            TextView textView2=allahrow.findViewById(R.id.rowBanglaDiscriptionlId);

            textView.setText(rowBanglaTitel[position]);
            textView1.setText(rowArabicTitell[position]);
            textView2.setText(rowBanglaDiscriptionl[position]);


            return allahrow;
        }
}
}
