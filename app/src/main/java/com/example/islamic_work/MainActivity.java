package com.example.islamic_work;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
ListView listView;
String [] titels;
String [] discriptions;
int [] image={R.drawable.image1,R.drawable.image2,R.drawable.image3, R.drawable.image4,
        R.drawable.image5,R.drawable.image6,R.drawable.image7,R.drawable.image8};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView=findViewById(R.id.mainlistId);
        titels=getResources().getStringArray(R.array.titels);
        discriptions=getResources().getStringArray(R.array.discriptions);

        MyAdapter myAdapter=new MyAdapter(this,image,titels,discriptions);

        listView.setAdapter(myAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView <?> parent, View view, int position, long id) {

                if (position==0){
                    Intent i=new Intent(MainActivity.this,AllahName.class);
                    startActivity(i);
                }
                if (position==1){
                    Intent i=new Intent(MainActivity.this,MuhammadNames.class);
                    startActivity(i);                }
                if (position==2){
                    Intent i=new Intent(MainActivity.this,QuranPrays.class);
                    startActivity(i);                }
                if (position==3){
                    Intent i=new Intent(MainActivity.this,Kalmas.class);
                    startActivity(i);                }
                if (position==4){
                    Intent i=new Intent(MainActivity.this,PrayerTimes.class);
                    startActivity(i);                }
                if (position==5){
                    Intent i=new Intent(MainActivity.this,Tasbeeh_count.class);
                    startActivity(i);                }
                if (position==6){
                    Intent i=new Intent(MainActivity.this,QiblaCompass.class);
                    startActivity(i);                }
                if (position==7){
                    Intent i=new Intent(MainActivity.this,MuslimBabyName.class);
                    startActivity(i);                }
            }
        });
    }
    class MyAdapter extends ArrayAdapter<String>{
        Context context;
        int [] imgs;
        String[] myTitel;
        String[] myDiscription;


         MyAdapter(Context c,int [] images, String[] titels,String[] discriptions) {
            super(c, R.layout.row_activity,R.id.rowImageId,titels);
            this.context=c;
            this.imgs=images;
            this.myTitel=titels;
            this.myDiscription=discriptions;
        }

        @NonNull
        @Override
        public View getView(int position,@Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater=(LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row=layoutInflater.inflate(R.layout.row_activity,parent,false);
            ImageView imageView=row.findViewById(R.id.rowImageId);
            TextView textView=row.findViewById(R.id.rowTextTitelId);
            TextView textView1=row.findViewById(R.id.rowTextDiscriptionlId);

            imageView.setImageResource(image[position]);
            textView.setText(titels[position]);
            textView1.setText(discriptions[position]);

            return row;
        }
    }
}
